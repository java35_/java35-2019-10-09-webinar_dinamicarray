package inheritance_methods_override_overloading;

public class Child extends Parent {
	int childValue = 15;
	
	@Override
	public void print() {
		System.out.println("I am a Child!");
	}
}