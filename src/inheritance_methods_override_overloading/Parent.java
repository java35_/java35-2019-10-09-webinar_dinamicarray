package inheritance_methods_override_overloading;

public class Parent {
	int parentValue = 5;

	public void print() {
		System.out.println("I am a Parent!");
	}
}