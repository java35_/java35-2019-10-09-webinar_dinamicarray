package inheritance_methods_override_overloading;

public class InheritanceAppl {
	public static void main(String[] args) {
		Parent 	parent 	= new Parent();
		Child 	child 	= new Child();
		
		parent.print();
		
		child.print();
		
		
		method1("Hello");
		method1("Hi", "Hi");
	}
	
	static void method1(String str1) {
		System.out.println(str1);
	}
	
	static void method1(String str1, String str2) {
		System.out.println(str1);
		System.out.println(str2);
	}
	
	static void method1(String str1, int index) {
		System.out.println(str1);
		System.out.println(index);
	}
}