package inheritance_instanceof;

public class InheritanceAppl {
	public static void main(String[] args) {
		Parent 	parent 	= new Parent();
		Child 	child 	= new Child();
		Child2 	child2 	= new Child2();
		
		printValue(parent);
		printValue(child);
		printValue(child2);
	}
	
	static void printValue(Parent parent) {
		if (parent instanceof Child) {
			Child child = (Child)parent;
			System.out.println("Child: " + child.childValue);
		} else if (parent instanceof Child2) {
			Child2 child2 = (Child2)parent;
			System.out.println("Child2: " + child2.childValue);
		} else
			System.out.println("Wrong! This is a parent");
	}
}