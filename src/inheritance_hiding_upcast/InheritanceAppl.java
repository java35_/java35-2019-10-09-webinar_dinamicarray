package inheritance_hiding_upcast;

public class InheritanceAppl {
	public static void main(String[] args) {
		Parent 	parent 	= new Parent();
		Child 	child 	= new Child();
		
		
		parent = child;
		
		System.out.println(parent.var);
	}
}