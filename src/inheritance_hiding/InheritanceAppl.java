package inheritance_hiding;

public class InheritanceAppl {
	public static void main(String[] args) {
		Parent 	parent 	= new Parent();
		Child 	child 	= new Child();
		
		System.out.println("Parent: " + parent.var);
		
		System.out.println("Child: " + child.var);
	}
}