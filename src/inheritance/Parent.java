package inheritance;

public class Parent extends GrandParent {
	int value = 5;
	
	@Override
	public void print() {
		System.out.println("I am a Parent!");
	}
}