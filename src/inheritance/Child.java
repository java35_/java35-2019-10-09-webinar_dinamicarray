package inheritance;

public class Child extends Parent {
	int value = 15;
	
	@Override
	public void print() {
		System.out.println("I am a Child!");
	}
}