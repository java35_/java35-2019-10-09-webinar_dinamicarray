package inheritance;

public class InheritanceAppl {
	public static void main(String[] args) {
		GrandParent grandParent	= new GrandParent();
		Parent 		parent 		= new Parent();
		Child 		child 		= new Child();
	
		grandParent.print();
		System.out.println(grandParent.value);
		
		grandParent = parent;
		grandParent.print();
		System.out.println(grandParent.value);
		
		grandParent = child;
		grandParent.print();
		System.out.println(grandParent.value);
	}
}