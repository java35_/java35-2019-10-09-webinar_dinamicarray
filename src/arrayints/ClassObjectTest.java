package arrayints;

public class ClassObjectTest {
	public static void main(String[] args) {
		int a = 5;
		Integer b;
		
		// Автоупаковка
		b = a;
		
		System.out.println(b);
	
		// Upcasting
		Object obj = b;
		
		Integer c = (Integer)obj;
		
		System.out.println(c);
		
		Object d[] = {'s', 's', 'w'};
		
		
	}
}