package arrayints;

public class DinamicArray {
	private int[] array = {};
	
	public boolean add(int newValue) {
		int[] arrayTemp = new int[array.length + 1];
		
		// Copy old array to new array
		for (int i = 0; i < array.length; i++) {
			arrayTemp[i] = array[i];
		}
		// a [0][1][2]
		//    |  |  |
		// b [0][1][2][0]
		
		// Add new value into temp array
		arrayTemp[arrayTemp.length - 1] = newValue;
		// b [0][1][2][newValue]
		
		array = arrayTemp;
		
		return true;
	}
	
	public boolean remove(int index) {
		if (index < 0 || index > array.length - 1)
			return false;
		
		int[] arrayTemp = new int[array.length - 1];
		
		// [][][][][]
		//     ^
		// [][][][][]
		//         ^
		
		for (int i = 0; i < index; i++) {
			arrayTemp[i] = array[i];
		}
		
		//  0  1  2  3  4
		// [0][1][2][3][5]
		//        ^
		//  0  1  2  3
		// [0][1][3][5]
		
		if (index != array.length - 1) {
			for (int i = index + 1; i < array.length; i++) {
				arrayTemp[i - 1] = array[i];
			}
		}
		array = arrayTemp;
		
		return true;
	}
	
	public int get(int index) {
		if (index < 0 || index > array.length - 1)
			return Integer.MIN_VALUE;
		
		return array[index];
	}
	
	public int size() {
		return array.length;
	}
}
