package arrayints;

public class DinamicArrayTest {
	public static void main(String[] args) {
		DinamicArray da = new DinamicArray();
		
		da.add(0);
		da.add(1);
		da.add(2);
		da.add(3);
		
		for (int i = 0; i < da.size(); i++) {
			System.out.println(da.get(i));
		}
		
		da.remove(2);
		
		for (int i = 0; i < da.size(); i++) {
			System.out.println(da.get(i));
		}
	}
}