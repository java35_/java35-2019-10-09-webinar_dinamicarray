package arrayints;

public class DinamicArrayAppl {
	public static void main(String[] args) {
		int[] a = {0, 1, 2};
		
		for (int i : a) {
			System.out.println(i);
		}
		System.out.println();
	
		a = new int[4];
		
		a[0] = 0;
		a[1] = 1;
		a[2] = 2;
		a[3] = 3;
		
		for (int i : a) {
			System.out.println(i);
		}
		
		// a [0][1][2]
		//    |  |  |
		// b [0][1][2][newValue]
		//
		// a = b;
		
		System.out.println("******************");
		a = add(a, 5);
		
		for (int i : a) {
			System.out.println(i);
		}
		
		System.out.println("*********************");
		a = remove(a, 2);
		
		for (int i : a) {
			System.out.println(i);
		}
	}

	static int[] remove(int[] array, int index) {
		
		if (index < 0 || index > array.length - 1)
			return array;
		
		int[] arrayTemp = new int[array.length - 1];
		
		// [][][][][]
		//     ^
		// [][][][][]
		//         ^
		
		for (int i = 0; i < index; i++) {
			arrayTemp[i] = array[i];
		}
		
		//  0  1  2  3  4
		// [0][1][2][3][5]
		//        ^
		//  0  1  2  3
		// [0][1][3][5]
		
		if (index != array.length - 1) {
			for (int i = index + 1; i < array.length; i++) {
				arrayTemp[i - 1] = array[i];
			}
		}
		
		return arrayTemp;
	}

	static int[] add(int[] a, int newValue) {
		int[] arrayTemp = new int[a.length + 1];
		
		for (int i = 0; i < a.length; i++) {
			arrayTemp[i] = a[i];
		}
		// a [0][1][2]
		//    |  |  |
		// b [0][1][2][0]
		
		arrayTemp[arrayTemp.length - 1] = newValue;
		// b [0][1][2][newValue]
		
		return arrayTemp;
	}
}