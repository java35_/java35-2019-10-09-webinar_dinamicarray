package inheritance_downcasting;

public class InheritanceAppl {
	public static void main(String[] args) {
		Parent 	parent 	= new Parent();
		Child 	child 	= new Child();
		
		System.out.println("Parent: " + parent.parentValue);
		
		System.out.println("Child: " + child.parentValue);
		System.out.println("Child: " + child.childValue);
		
		// Parent cannot be cast to Child
//		child = (Child)parent;
		
		parent = child;
		
		child = (Child)parent;
		
		System.out.println(parent.parentValue);
	}
	
	
}