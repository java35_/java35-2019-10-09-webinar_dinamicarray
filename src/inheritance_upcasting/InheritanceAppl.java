package inheritance_upcasting;

public class InheritanceAppl {
	public static void main(String[] args) {
		Parent 	parent 	= new Parent();
		Child 	child 	= new Child();
		
		System.out.println("Parent: " + parent.parentValue);
		
		System.out.println("Child: " + child.parentValue);
		System.out.println("Child: " + child.childValue);
		
		parent = child;
		
		System.out.println(parent.parentValue);
	}
}