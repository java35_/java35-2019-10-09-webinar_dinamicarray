package array;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MyArrayTest 
{
	DinamicArray number;
	DinamicArray str;
	
	Integer[] arNum = {10, 12, -1, 4, 6, 10};
	String[] arStr = {"auj", "hello", "aaa", "aaa", "c"};
	
	@Before
	public void setUp() throws Exception 
	{
		number = new DinamicArray();
		str = new DinamicArray();
		
		for(int i=0; i<arNum.length; i++)
		{
			number.add(arNum[i]);
		}
		
		for(int i=0; i<arStr.length; i++)
		{
			str.add(arStr[i]);
		}
	}

	@Test
	public void setUpTest() 
	{
		assertEquals(arNum.length, number.size());
		assertEquals(arStr.length, str.size());
		
		for(int i=0; i<arNum.length; i++)
		{
			assertEquals(arNum[i], number.get(i));
		}
		
		for(int i=0; i<str.size(); i++)
		{
			assertEquals(arStr[i], str.get(i));
		}
		
		assertNotEquals(arNum.length, str.size());
		assertFalse(str.add(null));
		assertNull(number.get(-1));
		assertNull(str.get(100));
	}

	
	
	
	
	
	
}
