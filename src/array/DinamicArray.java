package array;

public class DinamicArray {
	
	private static final int EXTENSIBILITY 		= 2;
	private static final int INICIAL_CAPACITY 	= 16;
	
	private Object[] 	array 		= new Object[INICIAL_CAPACITY];
	private int 		size 		= 0;
	private int 		capacity 	= INICIAL_CAPACITY;
	
	public boolean add(Object newValue) {
		if (newValue == null)
			return false;
		
		if (size == capacity - 1)
			allocateMemory();
		
		array[size++] = newValue;
		
		return true;
	}
	
	public boolean remove(int index) {
		if (index < 0 || index > size - 1)
			return false;
		
		// [0][1][2][3][4]
		//		  ^
		
		// index == 0
		// 0 < index < size
		// index == size
		
		
		if (index != capacity - 1) {
			for (int i = index; i < size; i++) {
				array[i] = array[i + 1];
			}
		}
		
		
		size--;
		return true;
	}
	
	public Object get(int index) {
		if (index < 0 || index > array.length - 1)
			return null;
		
		return array[index];
	}
	
	public int size() {
		return size;
	}
	
	// Private *******************************************************
	private void allocateMemory() {
		Object arrayTemp[] = new Object[capacity * EXTENSIBILITY];
		
		for (int i = 0; i < array.length; i++) {
			arrayTemp[i] = array[i];
		}
		
		capacity = arrayTemp.length;
		array = arrayTemp;
	}
}
