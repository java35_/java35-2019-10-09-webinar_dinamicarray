package inheritance_methods_intro;

public class Parent {
	int parentValue = 5;

	public void printParent() {
		System.out.println("I am a Parent!");
	}
}