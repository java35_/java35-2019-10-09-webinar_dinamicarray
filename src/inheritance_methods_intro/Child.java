package inheritance_methods_intro;

public class Child extends Parent {
	int childValue = 15;
	
	public void printChild() {
		System.out.println("I am a Child!");
	}
	
}
