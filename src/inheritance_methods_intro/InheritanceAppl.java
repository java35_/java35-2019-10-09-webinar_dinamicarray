package inheritance_methods_intro;

public class InheritanceAppl {
	public static void main(String[] args) {
		Parent 	parent 	= new Parent();
		Child 	child 	= new Child();
		
		parent.printParent();
		
		child.printParent();
		child.printChild();
		
	}
}